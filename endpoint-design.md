
FORMAT: 1A

# Users

API for users interaction.

## User Endpoints [/user]

### User List Info [GET]

```apib
+ Response 200 (application/json)

    { 
        "username": "j.doe",
        "email": "jane.doe@company.com"
    }
```

### User Create [POST]

```apib
+ username (string) - Name of user
+ first_name (string) - User first name
+ last_name (string) - User last name
+ email (string) - User email contact

+ Request (application/json)

    { 
        "username": "Set your username.", 
        "first_name": "What is your first name?", 
        "last_name": "What is you last name?"
        "email": "Insert your email contact."
    }         
```

```apib
+ Response 201 (application/json)
    
    + Body

        { 
            "username": "j.doe",
            "first_name": "Jane", 
            "last_name": "Doe"
            "email": "jane.doe@company.com" 
        }        
```


