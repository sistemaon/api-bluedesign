# api-bluedesign

api-bluedesign 

---

# Basic projectXYZ API
Welcome to the **projectXYZ** API. This API provides access to the **projectXYZ** service, pattern and structure.

# Status Code HTTP

code | reason | description | spec
---: | :----- | :---------- | :---
`200` | OK | Indicates that the request has succeeded. | [RFC7231#6.3.1](https://tools.ietf.org/html/rfc7231#section-6.3.1),<br>[RFC2616#10.2.1](https://tools.ietf.org/html/rfc2616#section-10.2.1)
`201` | Created | Indicates that the request has been fulfilled and has resulted in one or more new resources being created. | [RFC7231#6.3.2](https://tools.ietf.org/html/rfc7231#section-6.3.2),<br>[RFC2616#10.2.2](https://tools.ietf.org/html/rfc2616#section-10.2.2)